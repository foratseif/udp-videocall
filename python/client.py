#!/bin/env python3
import sys
import time
import socket
import camera
import numpy as np

SHAPE=(56,56,3)

client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
client_socket.settimeout(1.0)

msg_size=40960
#msg_size=15

message = b'shit'

if len(sys.argv) < 3:
    print(sys.argv[0], "<ip-address>", "<port>")
    exit(1)

args = sys.argv[1:3]
addr = (args[0], int(args[1]))

#start = time.time()
client_socket.sendto(message, addr)

buf = bytearray(msg_size)
mat = np.zeros(SHAPE, dtype=np.uint8)

while True:
    try:
        #nbytes, server = client_socket.recvfrom_into(buf, msg_size)
        nbytes, server = client_socket.recvfrom_into(mat.data, mat.nbytes)
        #print(nbytes, f'{buf}')
        if camera.my_imshow(mat, biggest_size=480):
            break
        #print(f'{data}')

    #    end = time.time()
    #    elapsed = end - start
    #    print(f'{data} {pings} {elapsed}')
    except socket.timeout:
        print('REQUEST TIMED OUT .. reconnecting ...')
        client_socket.sendto(message, addr)
    except:
        print("Other exception .. quitting ...")
        break

# De-allocate any associated memory usage
camera.destroyAllWindows()
