#!/bin/env python3
import random
import socket
import numpy as np
import camera

server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_socket.bind(('', 6969))

mat = np.zeros((112, 112, 3), dtype=np.uint8)
mat[10:102, 10:11, :] = 255

while True:
    message, address = server_socket.recvfrom(1)

    for img in camera.get_img(max_axis=75):
        #print(img.shape)
        server_socket.sendto(img.data, address)

    #message = message.upper()
    #server_socket.sendto(message, address)
