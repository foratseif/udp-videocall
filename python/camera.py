#!/bin/env python3
import sys
import numpy as np
import cv2

glob_max_axis = 100


def destroyAllWindows():
    cv2.destroyAllWindows()

def my_imshow(frame, biggest_size=0, events={}, timeout=1):
    if biggest_size > 0:
        h, w = frame.shape[:2]
        f=0
        if h < w:
            f=biggest_size/w
        else:
            f=biggest_size/h

        frame = cv2.resize(frame, (0, 0), fx=f, fy=f, interpolation=cv2.INTER_NEAREST)

    # Display the frame
    cv2.imshow('Camera', frame)

    # Wait for 25ms
    key = cv2.waitKey(timeout) & 0xFF
    if key == ord('q'):
        return True
    elif key == ord('s'):
        cv2.imwrite("pic.jpg", frame)

    for _, x in enumerate(events):
        if key == ord(x):
            events[x]()

    return False


def main(max_axis=150, colors=64, grayscale=False):
    global glob_max_axis

    glob_max_axis = max_axis
    print("glob_max_axis:", glob_max_axis)

    cap = cv2.VideoCapture(0)

    def inc_max_axis():
        global glob_max_axis
        glob_max_axis += 5
        print(glob_max_axis)

    def dec_max_axis():
        global glob_max_axis
        glob_max_axis -= 5
        glob_max_axis = max(glob_max_axis, 5)
        print(glob_max_axis)

    events = {
        '+': inc_max_axis,
        '-': dec_max_axis
    }

    # loop runs if capturing has been initialized
    while(1):
        m_axis = glob_max_axis

        # reads frame from a camera
        ret, frame = cap.read()

        h, w, d = frame.shape
        nh = nw = m_axis

        # if width is bigger
        if h > w:
            nw = round(m_axis * (w/h))
        else:
            nh = round(m_axis * (h/w))

        frame = cv2.resize(frame, (nw, nh), interpolation=cv2.INTER_LINEAR)

        # cut to ratio
        ratio = 1
        sml = min(nw, nh)
        off_x = (nw-sml)//2
        off_y = (nh-sml)//2
        frame = frame[off_y:off_y+sml, off_x:off_x+sml,:]

        # mirror
        frame = np.flip(frame, axis=1)

        # grayscale:
        if grayscale:
            frame = np.mean(frame, axis=2)

        # 16 colors
        frame = np.floor(frame/(256/colors))*(256/colors)

        # as type uint8
        frame = frame.astype(np.uint8)

        #print(frame.nbytes, frame.nbytes / 256 * colors, frame.shape)

        #yield frame

        # show frame
        if my_imshow(frame, biggest_size=480, events=events, timeout=10):
            break


    # release the camera from video capture
    cap.release()

    # De-allocate any associated memory usage
    cv2.destroyAllWindows()


def get_img(max_axis=100, colors=64, grayscale=False):
    cap = cv2.VideoCapture(0)

    # loop runs if capturing has been initialized
    while(1):
        # reads frame from a camera
        ret, frame = cap.read()

        h, w, d = frame.shape
        nh = nw = max_axis

        # if width is bigger
        if h > w:
            nw = round(max_axis * (w/h))
        else:
            nh = round(max_axis * (h/w))

        frame = cv2.resize(frame, (nw, nh), interpolation=cv2.INTER_LINEAR)

        # cut to ratio
        ratio = 1
        sml = min(nw, nh)
        off_x = (nw-sml)//2
        off_y = (nh-sml)//2
        frame = frame[off_y:off_y+sml, off_x:off_x+sml,:]

        # mirror
        #frame = np.flip(frame, axis=1)

        # grayscale:
        if grayscale:
            frame = np.mean(frame, axis=2)

        # 16 colors
        #frame = np.floor(frame/(256/colors))*(256/colors)

        # as type uint8
        frame = frame.astype(np.uint8)

        #print(frame.nbytes, frame.nbytes / 256 * colors, frame.shape)
        print(frame.shape)

        yield frame

    # release the camera from video capture
    cap.release()


def main(max_axis=150, colors=64, grayscale=False, timeout=1):
    global glob_max_axis

    glob_max_axis = max_axis
    print("glob_max_axis:", glob_max_axis)

    cap = cv2.VideoCapture(0)

    def inc_max_axis():
        global glob_max_axis
        glob_max_axis += 5
        print(glob_max_axis)

    def dec_max_axis():
        global glob_max_axis
        glob_max_axis -= 5
        glob_max_axis = max(glob_max_axis, 5)
        print(glob_max_axis)

    events = {
        '+': inc_max_axis,
        '-': dec_max_axis
    }

    # loop runs if capturing has been initialized
    while(1):
        m_axis = glob_max_axis

        # reads frame from a camera
        ret, frame = cap.read()

        h, w, d = frame.shape
        nh = nw = m_axis

        # if width is bigger
        if h > w:
            nw = round(m_axis * (w/h))
        else:
            nh = round(m_axis * (h/w))

        frame = cv2.resize(frame, (nw, nh), interpolation=cv2.INTER_LINEAR)

        # cut to ratio
        ratio = 1
        sml = min(nw, nh)
        off_x = (nw-sml)//2
        off_y = (nh-sml)//2
        frame = frame[off_y:off_y+sml, off_x:off_x+sml,:]

        # mirror
        frame = np.flip(frame, axis=1)

        # grayscale:
        if grayscale:
            frame = np.mean(frame, axis=2)

        # 16 colors
        frame = np.floor(frame/(256/colors))*(256/colors)

        # as type uint8
        frame = frame.astype(np.uint8)

        #print(frame.nbytes, frame.nbytes / 256 * colors, frame.shape)

        #yield frame

        # show frame
        if my_imshow(frame, biggest_size=480, events=events, timeout=timeout):
            break


    # release the camera from video capture
    cap.release()

    # De-allocate any associated memory usage
    cv2.destroyAllWindows()


if __name__ == "__main__":
    arg_name = ['max_axis', 'colors', 'timeout', 'grayscale']
    arg_type = [int, int, int, bool]
    arg_dict = {}

    sys.argv.pop(0)
    for i in range(len(arg_name)):
        if len(sys.argv) == 0:
            break
        arg_dict[arg_name[i]] = arg_type[i](sys.argv.pop(0))

    print(arg_dict)
    main(**arg_dict)
